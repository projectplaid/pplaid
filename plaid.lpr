program plaid;

uses
    Sysutils,
    arch, board, delays, semihosting
    ;

Var
  A : Array of TByteArray;
begin
  WriteLn('Hello world from UART WriteLn');

   Arch_Init;
   Board_Init;

   SetLength(A, 100);

  SemihostingWrite('Hello world from semihosting');

  //CEL := GetCurrentEL();
  //Write('CurrentEL = ');
  //Write(CEL);
  //WriteLn;
end.

