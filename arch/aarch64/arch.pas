unit arch;

{$mode ObjFPC}{$H+}

interface

uses
  //Classes,
  SysUtils;

procedure Arch_Init;
function GetCurrentEL: UInt64;

implementation

procedure Arch_Init;
begin

end;

function GetCurrentEL: UInt64;
var
  Res: UInt64;
begin
  asm
           MRS     x0, CurrentEL
           LSR     x0, x0, #2
           LDR     x0, Res
  end;
  GetCurrentEL := Res;
end;

end.

