unit semihosting;

{$mode ObjFPC}{$H+}

interface

//uses
//  Classes, SysUtils;

type
  SemihostOp = (
    SYS_OPEN = $01,
    SYS_CLOSE = $02,
    SYS_WRITEC = $03,
    SYS_WRITE0 = $04,
    SYS_WRITE = $05,
    SYS_READ = $06,
    SYS_READC = $07,
    SYS_ISERROR = $08,
    SYS_ISTTY = $09,
    SYS_SEEK = $0a,
    SYS_FLEN = $0c,
    SYS_TMPNAM = $0d,
    SYS_REMOVE = $0e,
    SYS_RENAME = $0f,
    SYS_CLOCK = $10,
    SYS_TIME = $11,
    SYS_SYSTEM = $12,
    SYS_ERRNO = $13,
    SYS_GET_CMDLINE = $15,
    SYS_HEAPINFO = $16,
    SYS_EXIT = $18,
    SYS_EXIT_EXTENDED = $20,
    SYS_ELAPSED = $30,
    SYS_TICKFREQ = $31);

const
  ApplicationExit = $20026;

procedure SemihostingCall(Op: SemihostOp; Args: UIntPtr);
procedure SemihostingWrite(S: String);

implementation

procedure SemihostingCall(Op: SemihostOp; Args: UIntPtr); assembler;
asm
    hlt #0xF000
end;

procedure SemihostingWrite(S: String);
var
  P: PChar;
begin
  P := PChar(S);
  SemihostingCall(SYS_WRITE0, UIntPtr(P));
end;

end.
